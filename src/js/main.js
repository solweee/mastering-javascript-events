import SectionCreator from "./join-us-section.js";
document.addEventListener("DOMContentLoaded", function () {
  const sectionFactory = new SectionCreator();
  const newSection = sectionFactory.create("standard");
  return newSection;
});
